#include <stdio.h>
#include <stdlib.h>

#include "JWArray_float.h"

//////////////////////////////////////////////////////////////////////////
//基础线性表函数
//////////////////////////////////////////////////////////////////////////

/************************************************************************/
/* 创建指定大小的顺序线性表，nIncrSize参数用于当存储空间不够时重新分配存储    */
/************************************************************************/
JWArray_float* JWArrayCreate_float(const int nInitSize, const int nIncrSize)
{
	JWArray_float *pArray;

	if (NULL == (pArray = (JWArray_float *)malloc(sizeof(JWArray_float))))
	{
		return NULL;
	}

	if (NULL == (pArray->pElem = (JWArrayElem_float *)malloc(nInitSize * sizeof(JWArrayElem_float))))
	{
		free(pArray);

		return NULL;
	}

	pArray->nLength		= 0;
	pArray->nTotalSize	= nInitSize;
	pArray->nIncrSize	= nIncrSize;

	return pArray;
}

/************************************************************************/
/* 销毁线性表                                                                   */
/************************************************************************/
void JWArrayDestroy_float(JWArray_float *pArray)
{
	if (NULL != pArray)
	{
		if (NULL != pArray->pElem)
		{
			free(pArray->pElem);
		}

		free(pArray);
	}
}

/************************************************************************/
/* 清空线性表                                                                   */
/************************************************************************/
void JWArrayMakeEmpty_float( JWArray_float *pArray )
{
	pArray->nLength = 0;
}

JWArray_BOOL JWArrayIsEmpty_float(JWArray_float *pArray)
{
	return pArray->nLength == 0 ? JWARRAY_TRUE : JWARRAY_FALSE;
}

JWArray_BOOL JWArrayIsFull_float(JWArray_float *pArray)
{
	return pArray->nLength >= pArray->nTotalSize ? JWARRAY_TRUE : JWARRAY_FALSE;
}

int JWArrayGetLength_float(JWArray_float *pArray)
{
	return pArray->nLength;
}

/************************************************************************/
/* 获得指定索引(0~长度-1)的值									             */
/************************************************************************/
JWArray_BOOL JWArrayGetAt_float(JWArray_float *pArray, const int nIndex, JWArrayElem_float *pElem)
{
	//判断输入索引是否合法,包含表为空的情况
	if (nIndex < 0 || nIndex > (pArray->nLength-1))
	{
		return JWARRAY_FALSE;
	}

	//获得当前索引的值
	*pElem =  pArray->pElem[nIndex];

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 设置指定索引(0~长度-1)的值									             */
/************************************************************************/
JWArray_BOOL JWArraySetAt_float(JWArray_float *pArray, const int nIndex, const JWArrayElem_float elem)
{
	//判断输入索引是否合法,包含表为空的情况
	if (nIndex < 0 || nIndex > (pArray->nLength-1))
	{
		return JWARRAY_FALSE;
	}

	//设置当前索引的值
	pArray->pElem[nIndex] = elem;

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 在指定索引(0~长度)前插入新值，索引==长度时表示在末尾添加新值                */
/************************************************************************/
JWArray_BOOL JWArrayInsert_float(JWArray_float *pArray, const int nIndex, const JWArrayElem_float elem)
{
	int		i;
	JWArrayElem_float *pNewElem;

	//判断输入索引是否合法
	if (nIndex < 0 || nIndex > pArray->nLength)
	{
		return JWARRAY_FALSE;
	}

	//如果表满则重新分配更大的表
	if (JWARRAY_TRUE == JWArrayIsFull_float(pArray))
	{
		pNewElem = (JWArrayElem_float *)realloc(pArray->pElem, (pArray->nTotalSize + pArray->nIncrSize) * sizeof(JWArrayElem_float));

		if (NULL == pNewElem)
		{
			return JWARRAY_FALSE;
		}
		else
		{
			pArray->pElem = pNewElem;
			pArray->nTotalSize += pArray->nIncrSize;
		}
	}

	//指定位置插入值
	for (i = pArray->nLength; i > nIndex; i--)
	{
		pArray->pElem[i] = pArray->pElem[i-1];
	}
	pArray->pElem[nIndex] = elem;
	pArray->nLength++;

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 同样是插入，保证插入后的结果按照pCompare来排列			                  */
/************************************************************************/
JWArray_BOOL JWArrayOrderInsert_float(JWArray_float *pArray, const JWArrayElem_float elem, JWArray_CompareFunc_float pCompare)
{
	int		i, nIndex;
	JWArrayElem_float *pNewElem;

	//找到要插入的位置
	nIndex = -1;
	for (i = 0; i < pArray->nLength -1; i++)
	{
		if (JWARRAY_TRUE  == pCompare(pArray->pElem[i], elem) &&
			JWARRAY_FALSE == pCompare(pArray->pElem[i+1], elem))//注意双边条件的判断
		{
			nIndex = i;
			break;
		}
	}
	if (nIndex == -1 && pArray->nLength >= 1)
	{
		if (JWARRAY_TRUE  == pCompare(pArray->pElem[pArray->nLength -1], elem))//单独考虑最后一个元素的比较
		{
			nIndex = pArray->nLength -1;
		}
	}
	nIndex++;//找到的索引位置后插入，没有找到满足条件或长度不为0则在第一个位置插入

	//如果表满则重新分配更大的表
	if (JWARRAY_TRUE == JWArrayIsFull_float(pArray))
	{
		pNewElem = (JWArrayElem_float *)realloc(pArray->pElem, (pArray->nTotalSize + pArray->nIncrSize) * sizeof(JWArrayElem_float));

		if (NULL == pNewElem)
		{
			return JWARRAY_FALSE;
		}
		else
		{
			pArray->pElem = pNewElem;
			pArray->nTotalSize += pArray->nIncrSize;
		}
	}

	//指定位置插入值
	for (i = pArray->nLength; i > nIndex; i--)
	{
		pArray->pElem[i] = pArray->pElem[i-1];
	}
	pArray->pElem[nIndex] = elem;
	pArray->nLength++;

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 删除指定索引的值(0~长度-1)                                             */
/************************************************************************/
JWArray_BOOL JWArrayDelete_float(JWArray_float *pArray, const int nIndex, JWArrayElem_float *pElem)
{
	int i;

	//判断输入索引是否合法,包含表为空的情况
	if (nIndex < 0 || nIndex > (pArray->nLength-1))
	{
		return JWARRAY_FALSE;
	}

	//删除指定位置的值
	if (NULL != pElem)
	{
		*pElem = pArray->pElem[nIndex];
	}

	for (i = nIndex; i < (pArray->nLength-1); i++)
	{
		pArray->pElem[i] = pArray->pElem[i+1];
	}
	pArray->nLength--;

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 在线性表中遍历找到满足pCompare函数的索引返回索引号，没找到则返回-1        */
/************************************************************************/
int JWArrayLocate_float(JWArray_float *pArray, const JWArrayElem_float elem, JWArray_CompareFunc_float pCompare)
{
	int i;

	for (i = 0; i < pArray->nLength; i++)
	{
		if (JWARRAY_TRUE == pCompare(pArray->pElem[i], elem))//找到满足要求的索引项
		{
			return i;
		}
	}

	return -1;
}

/************************************************************************/
/* 在线性表中遍历对每个遍历到的元素调用pTraverse函数				         */
/************************************************************************/
void JWArrayTraverse_float(JWArray_float *pArray, JWArray_TraverseFunc_float pTraverse)
{
	int i;

	for (i = 0; i < pArray->nLength; i++)
	{
		pTraverse(&(pArray->pElem[i]));
	}
}

/************************************************************************/
/* 定义单个元素的输出，对不同的JWArrayElem_float输出要更改该参数,主要用于调试       */
/************************************************************************/
JWArray_BOOL JWArrayPrintfElem_float(JWArrayElem_float *pElem)
{
	printf("%f\t", *pElem);

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 输出当前线性表的详情，主要用于调试                                           */
/************************************************************************/
void JWArrayDump_float(JWArray_float *pArray)
{
	int i;

	printf("当前顺序表元素为float类型\n");

	//判断是否为空
	if (pArray->nLength == 0)
	{
		printf("当前顺序表为空\n\n");
		return;
	}

	//打印当前顺序线性表数据
	printf("当前顺序表值列表详情如下:\n");
	printf("是否为空:%s\n", JWARRAY_TRUE == JWArrayIsEmpty_float(pArray) ? "空" : "非空");
	printf("是否为满:%s\n", JWARRAY_TRUE == JWArrayIsFull_float(pArray) ? "满" : "非满");
	printf("长度:%d\n", JWArrayGetLength_float(pArray));
	printf("值列表(下标从0~...):\n");
	for (i = 0; i < pArray->nLength; i++)
	{
		JWArrayPrintfElem_float(&(pArray->pElem[i]));

		//每五个数据一行
		if ((i+1)%5 == 0)
		{
			printf("\n");
		}
	}
	printf("\n\n");
}

//////////////////////////////////////////////////////////////////////////
//栈操作函数
//////////////////////////////////////////////////////////////////////////

/************************************************************************/
/* 获得当前栈顶元素的值
** 下标最大的为栈顶元素													*/
/************************************************************************/
JWArray_BOOL JWArrayGetTop_float(JWArray_float *pArray, JWArrayElem_float *pElem)
{
	//判断栈是否为空
	if (0 == pArray->nLength)
	{
		return JWARRAY_FALSE;
	}

	//获得当前栈顶的值
	*pElem =  pArray->pElem[pArray->nLength - 1];

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 设置当前栈顶元素的值
** 下标最大的为栈顶元素													*/
/************************************************************************/
JWArray_BOOL JWArraySetTop_float(JWArray_float *pArray, const JWArrayElem_float elem)
{
	//判断栈是否为空
	if (0 == pArray->nLength)
	{
		return JWARRAY_FALSE;
	}

	//获得当前栈顶的值
	pArray->pElem[pArray->nLength - 1] = elem;

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 当前栈入栈一个元素										                  */
/************************************************************************/
JWArray_BOOL JWArrayPush_float(JWArray_float *pArray, const JWArrayElem_float elem)
{
	JWArrayElem_float *pNewElem;

	//如果栈满则重新分配更大的栈
	if (JWARRAY_TRUE == JWArrayIsFull_float(pArray))
	{
		pNewElem = (JWArrayElem_float *)realloc(pArray->pElem, (pArray->nTotalSize + pArray->nIncrSize) * sizeof(JWArrayElem_float));

		if (NULL == pNewElem)
		{
			return JWARRAY_FALSE;
		}
		else
		{
			pArray->pElem = pNewElem;
			pArray->nTotalSize += pArray->nIncrSize;
		}
	}

	//末尾位置插入值
	pArray->pElem[pArray->nLength++] = elem;

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 当前栈出栈一个元素			                                                 */
/************************************************************************/
JWArray_BOOL JWArrayPop_float(JWArray_float *pArray, JWArrayElem_float *pElem)
{
	//判断栈是否为空
	if (0 == pArray->nLength)
	{
		return JWARRAY_FALSE;
	}

	//栈顶出栈
	if (NULL != pElem)
	{
		*pElem = pArray->pElem[pArray->nLength-1];
	}
	pArray->nLength--;

	return JWARRAY_TRUE;
}

//////////////////////////////////////////////////////////////////////////
//队列操作函数
//////////////////////////////////////////////////////////////////////////

/************************************************************************/
/* 获得当前队列头元素的值
** 下标为0的为队列头元素													*/
/************************************************************************/
JWArray_BOOL JWArrayGetHead_float(JWArray_float *pArray, JWArrayElem_float *pElem)
{
	//判断队列是否为空
	if (0 == pArray->nLength)
	{
		return JWARRAY_FALSE;
	}

	//获得当前队列头元素的值
	*pElem =  pArray->pElem[0];

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 设置当前队列头元素的值
** 下标为0的为队列头元素													*/
/************************************************************************/
JWArray_BOOL JWArraySetHead_float(JWArray_float *pArray, const JWArrayElem_float elem)
{
	//判断队列是否为空
	if (0 == pArray->nLength)
	{
		return JWARRAY_FALSE;
	}

	//设置当前队列头元素的值
	pArray->pElem[0] = elem;

	return JWARRAY_TRUE;
}

/************************************************************************/
/* 当前队列入列一个元素--即为线性表末尾插入一个元素							*/
/************************************************************************/
JWArray_BOOL JWArrayEnQueue_float(JWArray_float *pArray, const JWArrayElem_float elem)
{
	JWArrayElem_float *pNewElem;

	//如果队列满则重新分配更大的队列
	if (JWARRAY_TRUE == JWArrayIsFull_float(pArray))
	{
		pNewElem = (JWArrayElem_float *)realloc(pArray->pElem, (pArray->nTotalSize + pArray->nIncrSize) * sizeof(JWArrayElem_float));

		if (NULL == pNewElem)
		{
			return JWARRAY_FALSE;
		}
		else
		{
			pArray->pElem = pNewElem;
			pArray->nTotalSize += pArray->nIncrSize;
		}
	}

	//末尾位置插入值
	pArray->pElem[pArray->nLength++] = elem;
	return JWARRAY_TRUE;
}

/************************************************************************/
/* 当前队列出列一个元素--即为删除线性表第一个元素			                  */
/************************************************************************/
JWArray_BOOL JWArrayDeQueue_float(JWArray_float *pArray, JWArrayElem_float *pElem)
{
	int i;

	//判断队列是否为空
	if (0 == pArray->nLength)
	{
		return JWARRAY_FALSE;
	}

	//出队列--删除第一个元素的值
	if (NULL != pElem)
	{
		*pElem = pArray->pElem[0];
	}

	for (i = 0; i < (pArray->nLength-1); i++)
	{
		pArray->pElem[i] = pArray->pElem[i+1];
	}
	pArray->nLength--;

	return JWARRAY_TRUE;
}