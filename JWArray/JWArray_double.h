#ifndef JWARRAY_DOUBLE_H_H_H
#define JWARRAY_DOUBLE_H_H_H

#include "JWArray_base.h"

typedef double				JWArrayElem_double;

typedef struct
{
	JWArrayElem_double	*pElem;			//数据元素存储区域
	int				nLength;		//当前数据元素长度
	int				nTotalSize;		//当前线性表长度
	int				nIncrSize;		//当线性表已满时，重新分配的新增区域大小
}JWArray_double;

typedef JWArray_BOOL (*JWArray_CompareFunc_double)(JWArrayElem_double elem1, JWArrayElem_double elem2);
typedef JWArray_BOOL (*JWArray_TraverseFunc_double)(JWArrayElem_double *pElem);

#ifdef __cplusplus
extern "C" 
{
#endif

	/************************************************************************/
	/* 基础线性表函数声明                                                           */
	/************************************************************************/
	JWArray_double* JWArrayCreate_double(const int nInitSize, const int nIncrSize);
	void JWArrayDestroy_double(JWArray_double *pArray);
	void JWArrayMakeEmpty_double(JWArray_double *pArray );

	JWArray_BOOL JWArrayIsEmpty_double(JWArray_double *pArray);
	JWArray_BOOL JWArrayIsFull_double(JWArray_double *pArray);
	int JWArrayGetLength_double(JWArray_double *pArray);

	JWArray_BOOL JWArrayGetAt_double(JWArray_double *pArray, const int nIndex, JWArrayElem_double *pElem);
	JWArray_BOOL JWArraySetAt_double(JWArray_double *pArray, const int nIndex, const JWArrayElem_double elem);
	JWArray_BOOL JWArrayInsert_double(JWArray_double *pArray, const int nIndex, const JWArrayElem_double elem
	JWArray_BOOL JWArrayOrderInsert_double(JWArray_double *pArray, const JWArrayElem_double elem, JWArray_CompareFunc_double pCompare);
	JWArray_BOOL JWArrayDelete_double(JWArray_double *pArray, const int nIndex, JWArrayElem_double *pElem);
	int JWArrayLocate_double(JWArray_double *pArray, const JWArrayElem_double elem, JWArray_CompareFunc_double pCompare);
	void JWArrayTraverse_double(JWArray_double *pArray, JWArray_TraverseFunc_double pTraverse);

	JWArray_BOOL JWArrayPrintfElem_double(JWArrayElem_double *pElem);
	void JWArrayDump_double(JWArray_double *pArray);

	/************************************************************************/
	/* 栈操作函数声明															*/
	/************************************************************************/
	JWArray_BOOL JWArrayGetTop_double(JWArray_double *pArray, JWArrayElem_double *pElem);
	JWArray_BOOL JWArraySetTop_double(JWArray_double *pArray, const JWArrayElem_double elem);
	JWArray_BOOL JWArrayPush_double(JWArray_double *pArray, const JWArrayElem_double elem);
	JWArray_BOOL JWArrayPop_double(JWArray_double *pArray, JWArrayElem_double *pElem);

	/************************************************************************/
	/* 队列操作函数声明													    */
	/************************************************************************/
	JWArray_BOOL JWArrayGetHead_double(JWArray_double *pArray, JWArrayElem_double *pElem);
	JWArray_BOOL JWArraySetHead_double(JWArray_double *pArray, const JWArrayElem_double elem);
	JWArray_BOOL JWArrayEnQueue_double(JWArray_double *pArray, const JWArrayElem_double elem);
	JWArray_BOOL JWArrayDeQueue_double(JWArray_double *pArray, JWArrayElem_double *pElem);

#ifdef __cplusplus
};
#endif

#endif